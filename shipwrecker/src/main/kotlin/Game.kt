import io.vertx.ext.web.client.WebClient

class Game {
//
//    private val myBoard = Board()
//    private val enemyBoard = Board()
//    private lateinit var myCell: Cell
//    private lateinit var enemyCell: Cell
    private val player_one = Player()
    private val player_two = Player()
//    private var turnsNumber: Int = 0
    private var winner = PLAYER_TWO
    private var status = GAME_STATUS_ONGOING
    fun start(){
        println(GAME_PLAYER_ONE_SHIPS_MESSAGE)
        while (player_one.Board.ships.count() != GAME_MAX_NUMBER_OF_SHIPS)
            placeShipOnBoard(player_one.Board)
        println(GAME_PLAYER_TWO_SHIPS_MESSAGE)
        while (player_two.Board.ships.count() != GAME_MAX_NUMBER_OF_SHIPS)
            placeShipOnBoard(player_two.Board)
//        while ((status == GAME_STATUS_ONGOING) || (turnsNumber == 201)){
//            println("My turn")
//            myTurn()
//            println("Enemy turn")
//            enemyTurn()
//        }
        while (status == GAME_STATUS_ONGOING){
            println("Player One's Turn")
            turnAgainst(player_two)
            if(status == GAME_STATUS_END){
                winner = PLAYER_ONE
            }
            println("Player Two's Turn")
            turnAgainst(player_one)
        }
        endGameMessage()
    }

    private fun placeShipOnBoard(board: Board){
        println(GAME_SHIP_SIZE_MESSAGE)
        val ship = Ship(readLine()!!.toInt())
        println(GAME_SHIP_HEAD_COORDINATES_MESSAGE)
        val (x: String, y: String, direction: String) = readLine()!!.split(" ")
        ship.make(Cell(x.toInt(), y.toInt()), direction)

        if(!board.addShip(ship))
            println(GAME_SHIP_NOT_ADDED_MESSAGE)
    }

//    private fun myTurn(){
//        //Удар по клетке которую выбрал враг
//        //Выбрать клетку по которой атакуем мы
//        // if (got win message) endGame return
//        turnsNumber++
//        if (::myCell.isInitialized) {
//            myBoard.fire(myCell)
//            checkGameStatusOn(myBoard)
//            if (status == GAME_STATUS_END)
//                winner = GAME_WINNER_PLAYER_TWO
//            //if(game status lost) endGAme sendEnemy win message return
//        }
//        println(GAME_CELL_ATTACK_MESSAGE)
//        val (x: String, y: String) = readLine()!!.split(" ")
//        enemyCell = Cell(x.toInt(), y.toInt())
//    }
//
//    private fun enemyTurn(){
//       //Получаем ответочку от врага
//       //Не сдох ли он уже
//       //Если нет то какую клетку атакует
//       //Какую клетку он атакует
//       //Отправляем врагу клетку которую атакуем
//        turnsNumber++
//        if (::enemyCell.isInitialized) {
//            enemyBoard.fire(enemyCell)
//            checkGameStatusOn(enemyBoard)
//            if (status == GAME_STATUS_END)
//                winner = GAME_WINNER_PLAYER_ONE
//            //if(game status lost) endGAme sendEnemy win message return
//        }
//        println(GAME_CELL_ATTACK_MESSAGE)
//        val (x: String, y: String) = readLine()!!.split(" ")
//        myCell = Cell(x.toInt(), y.toInt())
//    }

    private fun turnAgainst(player: Player)
    {
        do {
            checkGameStatusOn(player.Board)
            if (status == GAME_STATUS_END)
                return
            println(GAME_CELL_ATTACK_MESSAGE)
            val (x: String, y: String) = readLine()!!.split(" ")
            player.attackedCell = Cell(x.toInt(), y.toInt())
            player.Board.fire(player.attackedCell)
        }
         while(player.attackedCell.status != CELL_STATUS_MISSED)
    }


    private fun checkGameStatusOn(board: Board) {
        if (board.ships.count { it.status == SHIP_STATUS_DEAD } == GAME_MAX_NUMBER_OF_SHIPS)
            status = GAME_STATUS_END
    }
//    private fun endGameMessage() {
//        if(status == GAME_STATUS_END)
//            if (winner == GAME_WINNER_PLAYER_ONE)
//                println(GAME_WIN_MESSAGE)
//            else
//                println(GAME_LOSE_MESSAGE)
//        else
//            println(GAME_ONGOING_ERROR_MESSAGE)
//    }
    private fun endGameMessage() {
        if (status == GAME_STATUS_END) {
            if(winner == PLAYER_ONE){
                println(PLAYER_ONE_GAME_WIN_MESSAGE)
                println(PLAYER_TWO_GAME_LOSE_MESSAGE)
            }
            else{
                println(PLAYER_TWO_GAME_WIN_MESSAGE)
                println(PLAYER_ONE_GAME_LOSE_MESSAGE)
            }
        } else {
            println(GAME_ONGOING_ERROR_MESSAGE)
        }
    }
}