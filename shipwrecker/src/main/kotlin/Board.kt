class Board {
    val ships = arrayListOf<Ship>()
    val firedCells = arrayListOf<Cell>()


    fun addShip(ship: Ship) : Boolean {
        if (checkShipSpace(ship)) {

            ships.add(ship)
            return true
        }
        return false
    }

    private fun checkShipSpace(ship: Ship): Boolean {
        for (shipOnBoard in ships){
            if (checkShipCollision(ship, shipOnBoard)){
                return false
            }
        }
        return true
    }

    private fun checkShipCollision(shipOne: Ship, shipTwo: Ship): Boolean {
        for (shipOneCell in shipOne.cells)
            for (shipTwoCell in shipTwo.cells) {
                if (shipOneCell.isNear(shipTwoCell))
                    return true
            }
        return false
    }

//    fun fire(cell: Cell): Int{
//        if (canFire(cell)){
//            for (ship in ships) {
//                if (ship.fire(cell) == CELL_STATUS_FIRED) {
//                    println("NOT VERY DEAD, but got a travma on ship's cell")
//                    ship.refreshShipStatus()
//                    return 1
//                }
//            }
//            if (cell.status != CELL_STATUS_FIRED) {
//                println("HAha, you MIMO")
//                cell.status = CELL_STATUS_MISSED
//                firedCells.add(cell)
//            }
//        }
//        return 0
//    }
    fun fire(cell: Cell): Int{
        if (canFire(cell)){
            for (ship in ships) {
                if (ship.fire(cell) == CELL_STATUS_FIRED) {
                    println("NOT VERY DEAD, but got a travma on ship's cell")
                    ship.refreshShipStatus()
                    firedCells.add(cell)
                    break
                }
            }
            if (cell.status != CELL_STATUS_FIRED) {
                println("HAha, you MIMO")
                cell.status = CELL_STATUS_MISSED
                firedCells.add(cell)
            }
            return 1;
        }
        return 0
    }
    private fun canFire(cell: Cell): Boolean {
        for (firedCell in firedCells)
            if (firedCell.equalsTo(cell))
                return false
        return true
    }
}