const val CELL_STATUS_EMPTY = "Empty"
const val CELL_STATUS_FIRED = "Fired"
const val CELL_STATUS_MISSED = "Missed"


const val BOARD_X_MIN = 1
const val BOARD_X_MAX = 10
const val BOARD_Y_MIN = 1
const val BOARD_Y_MAX = 10


const val SHIP_STATUS_ALIVE = "Alive"
const val SHIP_STATUS_DEAD = "Dead"

const val SHIP_DIRECTION_NORTH = "n"
const val SHIP_DIRECTION_EAST = "e"
const val SHIP_DIRECTION_WEST = "w"
const val SHIP_DIRECTION_SOUTH = "s"

const val GAME_MAX_NUMBER_OF_SHIPS = 10

const val GAME_STATUS_ONGOING = 0 //"Ongoing"
/*
const val GAME_STATUS_LOST = "Lost"
const val GAME_STATUS_WIN = "Win"
*/
const val GAME_STATUS_END = 1 //"Game ended."

const val GAME_SHIP_SIZE_MESSAGE = "Please enter ship size "
const val GAME_SHIP_HEAD_COORDINATES_MESSAGE = "Please enter ship head coordinates and its direction "
const val GAME_CELL_ATTACK_MESSAGE = "Please choose cell for attack "
const val GAME_SHIP_NOT_ADDED_MESSAGE = "Couldn't add ship there, try another cell"

const val PLAYER_ONE_GAME_WIN_MESSAGE = "Congratulations!!! You won the game!!"
const val PLAYER_ONE_GAME_LOSE_MESSAGE = "You lost <o_o>. How sad."

const val PLAYER_TWO_GAME_WIN_MESSAGE = "Hou, You did a great job defeating the enemy!!!"
const val PLAYER_TWO_GAME_LOSE_MESSAGE = "Oh, no! Miserable defeat, was it?"

const val GAME_ONGOING_ERROR_MESSAGE = "Oops! Game is still ongoing."

const val GAME_WINNER_WRONG = "Something went wrong, this is not a winner"
const val GAME_WINNER_PLAYER_ONE = "I'm the winner!!"
const val GAME_WINNER_PLAYER_TWO = "Enemy won, try again!!"

const val GAME_PLAYER_ONE_SHIPS_MESSAGE = "Player One, place the ships!!"
const val GAME_PLAYER_TWO_SHIPS_MESSAGE  = "Player Two, on Board!!"

const val  PLAYER_ONE = "Player One"
const val  PLAYER_TWO = "Player Two"